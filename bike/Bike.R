data <- read.csv('bike_train.csv', sep=",",header=TRUE) 

library(corrplot)
library(stringi)

data$dteday <- as.numeric(stri_sub(data$dteday, 9,10)) # On garde que le jour de la colonne date
names(data)[names(data) == "dteday"] <- "day" #On renomme la colonne en jour

data$season <- as.factor(data$season)
data$mnth <- as.factor(data$mnth)
data$holiday <- as.factor(data$holiday)
data$weekday <- as.factor(data$weekday)
data$workingday <- as.factor(data$workingday)
data$weathersit <- as.factor(data$weathersit)
data$day <- as.factor(data$day)


#Corrélation

############################
#Variable Quantitatives
############################
#Pearson 
#Corrplot pour les variables quantitatives
# mcor <- cor(data[,c("instant","temp","atemp","hum","windspeed","cnt")])
# corrplot(mcor, type="upper", order="hclust", tl.col="black", tl.srt=45)
# 
# panel.cor <- function(x, y){
#   usr <- par("usr"); on.exit(par(usr))
#   par(usr = c(0, 1, 0, 1))
#   r <- round(cor(x, y), digits=2)
#   txt <- paste0("R = ", r)
#   text(0.5, 0.5, txt)
# }
# # # Create the plots
# pairs(data[,c("instant","temp","atemp","hum","windspeed","cnt")], lower.panel = panel.cor)

############################
#Variable Qualitatives
############################
#Test chi2
# panel.cor.quali <- function(x, y){
#   usr <- par("usr"); on.exit(par(usr)) 
#   table.contigence <- table(x, y)
#   p_value<- chisq.test(table.contigence, simulate.p.value = TRUE)
#   par(usr = c(0, 1, 0, 1))
#   txt <- paste0("p_value = ", p_value$p.value)
#   text(0.5, 0.5, txt)
# }
# 
# pairs(data[,c("instant", "day","season","mnth","holiday","weekday","workingday", "weathersit")], lower.panel = panel.cor.quali, upper.panel=NULL)

############################
#Variable Quantitaitves-Qualitative
############################

 #Test ANOVA
# tableau <- data.frame(cnt=c(oneway.test(data$cnt~data$day)$p.value,
# oneway.test(data$cnt~data$season)$p.value,
# oneway.test(data$cnt~data$mnth)$p.value,
# oneway.test(data$cnt~data$holiday)$p.value,
# oneway.test(data$cnt~data$weekday)$p.value,
# oneway.test(data$cnt~data$workingday)$p.value,
# oneway.test(data$cnt~data$weathersit)$p.value))
# 
# rownames(tableau)<- c("day","season","mnth","holiday","weekday","workingday", "weathersit")
# 
# library(gridExtra)
# library(grid)
# 
# grid.table(tableau)


#########################
#Analyse de donnée - tendances
#########################
library(dplyr)

#Faire somme par mois

# data_mois <- data %>% group_by(mnth) %>% summarise(mnth = mnth[1], cnt=sum(cnt))
# barplot(data_mois$cnt,
#         main="Total bike by month during 2011",
#         ylab="Count",
#         border="red",
#         names.arg = c("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"),
#         col="blue",
#         density=10
# )

#Faire somme par saison
# data_saison <- data %>% group_by(season) %>% summarise(season = season[1], cnt=sum(cnt))
# barplot(data_saison$cnt,
#         main="Total bike by season during 2011",
#         ylab="Count",
#         border="red",
#         names.arg = c("Winter", "Spring", "Summer", "Fall"),
#         col="blue",
#         density=10
# )

#Faire somme par workingday

# data_workingday <- data %>% group_by(workingday) %>% summarise(workingday = workingday[1], cnt=sum(cnt))
# barplot(data_workingday$cnt,
#         main="Total bike by working day during 2011",
#         ylab="Count",
#         border="red",
#         names.arg = c("work", "no work"),
#         col="blue",
#         density=10
# )

#install.packages('FNN')  
#install.packages("glmnet", repos = "http://cran.us.r-project.org")
library(FNN)
library(leaps)
library(glmnet)
library(splines)
library(mgcv)
library(gam)
library(pls)
library(rpart)
set.seed(52)

# mix up observations randomly

#data <- subset(data, select=-c(yr, temp, holiday, weekday, workingday, day )) # On retire l'id et l'année 
data <- subset(data, select=-c(yr, day, instant)) # On retire l'id et l'année 

data <- data[sample(nrow(data)), ]

p <- ncol(data)-1
n <-  nrow(data)

group.outer <- rep((1:10), (n/10)+1)[1:n]
idx.test.outer <- list()
idx.test.inner <- list()
rs.data.inner <- list()
for(i in 1:10){
  index.cv <- which(group.outer==i)
  idx.test.outer[[i]] <- index.cv
  n.inner <- n - length(index.cv)
  rs.data.inner[[i]] <- sample(n.inner)
  group.inner <- rep((1:10), (n.inner/10)+1)[1:n.inner]
  idx.test.inner[[i]] <- list()
  for(j in 1:10){
    index.inner.cv <- which(group.inner==j)
    idx.test.inner[[i]][[j]] <- index.inner.cv
  }
}
# plot interval errors 
plot.cv.error <- function(data, x.title="x", ylim){
  ic.error.bar <- function(x, lower, upper, length=0.1){ 
    arrows(x, upper, x, lower, angle=90, code=3, length=length, col='red')
  }
  stderr <- function(x) sd(x)/sqrt(length(x))
  # calculer les erreurs moyennes et l'erreur type (standard error)
  means.errs <- colMeans(data)
  std.errs <- apply(data, 2, stderr)
  idx.best <- which(std.errs == min(std.errs))
  print(idx.best)
  # plotting  
  x.values <- 1:ncol(data)
  plot(x.values, means.errs, 
       ylim =ylim,
       xaxt="n",
       xlab=x.title, ylab='CV MSE')
  ic.error.bar(x.values, means.errs - 1.6*std.errs, means.errs + 1.6*std.errs)
  #abline(v=idx.best)
}


###################
#KNN nested cv  
#################### 
# Kmax <- 200
# K.sequences <- seq(1, Kmax, by = 2)
# err.knn.mse <-  rep(0, 10)
# err.knn.kmin <-  rep(0, 10)
# for(i in 1:10){
#   index.outer.cv <- idx.test.outer[[i]] # choix des index groupe exterieur (groupe de validation)
#   data.inner <- data[-index.outer.cv,]  # selection des données du groupe interieur
#   data.validation <- data[index.outer.cv,] # selection des données du groupe exterieur
#   # re-sampling (inner cross validation)
#   data.inner <- data.inner[rs.data.inner[[i]], ]
#   knn.mse.kmin <- rep(0, length(K.sequences)) # tableau qui va rassembler pour chaque K testé les MSE
#   for(k in 1:length(K.sequences)){ #on parcourt la liste des K 
#     for(j in 1:10){ # on effectue l'algorithme 1à fois dans le group interieur
#       index.inner.cv <- idx.test.inner[[i]][[j]]   # choix des index du groupe de test
#       data.train.x <- data.inner[-index.inner.cv, -ncol(data)]
#       data.train.y <- data.inner[-index.inner.cv, ncol(data)]
#       data.test.x <- data.inner[index.inner.cv, -ncol(data)]
#       data.test.y <- data.inner[index.inner.cv, ncol(data)]
#       model.reg <- knn.reg(train=data.train.x, 
#                            test=data.test.x, 
#                            y=data.train.y, k=K.sequences[k])
#       # on peut calculer la moyenne knn.mse.kmin 
#       # (par chaque k, mais ils sont proportionnels)
#       knn.mse.kmin[k] <- knn.mse.kmin[k] + mean((model.reg$pred-data.test.y)^2)  # on compare les sommes ou les additions, c'est pareil car 10 exemplaires
#     }
#   } 
#   idx.kmin <- which(min(knn.mse.kmin) == knn.mse.kmin)  # on récupère l'index qui correspond à la plus petite MSE en moyenne
#   best.kmin <- K.sequences[idx.kmin]  # on récupère le K qui correspond à cet index
#   err.knn.kmin[i] <- best.kmin
#   # validation our model with best model 
#   data.train.x <- data.inner[, -ncol(data)]
#   data.train.y <- data.inner[, ncol(data)]
#   data.test.x <- data.validation[, -ncol(data)]
#   data.test.y <- data.validation[, ncol(data)]
#   model.reg <- knn.reg(train=data.train.x, 
#                        test=data.test.x, 
#                        y=data.train.y, k=best.kmin)
#   err.knn.mse[i] <- mean((model.reg$pred-data.test.y)^2)  # on récupère pour chaque groupe exterieur la MSE testée 
#   # avec le groupe de validation, pour le meilleur hyperparametre K 
# }
# plot(x = K.sequences, y = knn.mse.kmin/10, type='l', ylab='Cv(k) Error', xlab="k voisins")
# boxplot(err.knn.mse, title = "MSE boxplot for KNN regression", horizontal = TRUE )
# mean(err.knn.mse)
# 
# 

###################
# linear regression 
####################
err.lin.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.train <- data[-index.outer.cv,]
  data.test <- data[index.outer.cv,]
  
  # linear regression
  # modeling with lm
  model.fit <- lm(cnt~., data=data.train)
  summary(model.fit)
  
  # prediction
  ypredi <- predict(model.fit, newdata=data.test)
  err.lin.mse[i] = mean((data.test$cnt -ypredi)^2)
  
}
#boxplot(err.lin.mse)
mean(err.lin.mse)



################
# REGULARIZATION 
################
# regression regularized   (elastic net)

n.alpha <- 25
err.ela.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  alphas <- seq(0, 1, length.out =  n.alpha)
  ela.inner.mse <- rep(0, n.alpha)
  best.lambda.alpha <- rep(0, n.alpha)
  for(k in 1:length(alphas)){
    for(j in 1:10){ 
      index.inner.cv <- idx.test.inner[[i]][[j]] 
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      
      alpha <- alphas[k]
      print(alpha)
      X.cv.train <- data.matrix(data.inner[, -ncol(data)])
      y.cv.train <- data.inner[, ncol(data)]
      cv.model <- cv.glmnet(X.cv.train, y.cv.train, alpha=alpha)
      ela.inner.mse[k] <- ela.inner.mse[k] + cv.model$cvm[which(cv.model$lambda == cv.model$lambda.min)]
    }
  }
  idx.best <- which(ela.inner.mse == min(ela.inner.mse))
  best.lambda <- best.lambda.alpha[idx.best]
  best.alpha <- alphas[idx.best]
  # validation model 
  X.train <- data.matrix(data.inner[, -ncol(data)])
  y.train <- data.inner[, ncol(data)]
  X.test <- data.matrix(data.validation[, -ncol(data)])
  y.test <- data.validation[, ncol(data)]
  cv.model <- cv.glmnet(X.train, y.train, alpha=best.alpha)
  model.fit <- glmnet(X.train, y.train, lambda = cv.model$lambda.min, alpha = best.alpha)
  err.ela.mse[i] <- mean((y.test - predict(model.fit, newx=X.test))^2)
}
plot(x = alphas, y = ela.inner.mse/10, type='l', ylab='Cv(alpha) Error', xlab="alpha")
boxplot(err.ela.mse)
mean(err.ela.mse)

# lasso
err.las.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  alpha <- 1
  
  # validation model 
  X.train <- data.inner[, -ncol(data)]
  y.train <- data.inner[, ncol(data)]
  X.test <- data.validation[, -ncol(data)]
  y.test <- data.validation[, ncol(data)]
  cv.model <- cv.glmnet(data.matrix(X.train), y.train, alpha = alpha)
  model.fit <- glmnet(data.matrix(X.train), y.train, lambda = cv.model$lambda.min, alpha = alpha)
  err.las.mse[i] <- mean((y.test - predict(model.fit, newx=data.matrix(X.test)))^2)
}

boxplot(err.las.mse)
mean(err.las.mse)

# ridge
err.rid.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  # re-sampling (inner cross validation)
  alpha <- 0
  
  # validation model 
  X.train <- data.matrix(data.inner[, -ncol(data)])
  y.train <- data.inner[, ncol(data)]
  X.test <- data.matrix(data.validation[, -ncol(data)])
  y.test <- data.validation[, ncol(data)]
  cv.model <- cv.glmnet(X.train, y.train, alpha = alpha)
  model.fit <- glmnet(X.train, y.train, lambda = cv.model$lambda.min, alpha = alpha)
  err.rid.mse[i] <- mean((y.test - predict(model.fit, newx=X.test))^2)
}
boxplot(err.rid.mse)
mean(err.rid.mse)

####################
#Principal Component regression (pcr)
#####################
err.pcr.mse<- rep(0, 10)
err.pcr.nopt <- rep(0,10)
nb.variables.max <- ncol(data) - 1
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,] 
  data.validation <- data[index.outer.cv,] 
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  pcr.errors <- matrix(NA, 10,  nb.variables.max)
  for(j in 1:10){ 
    index.inner.cv <- idx.test.inner[[i]][[j]] 
    data.train <- data[-index.inner.cv,]
    data.test <- data[index.inner.cv, ]
    # train pcr model
    model.pcr <- pcr(cnt~., data=data.train, scale=TRUE, validation="CV")
    # predict model fr different number of the best composants taken in considerations
    for(n in 1:nb.variables.max){
      y.pred <- predict(model.pcr, newdata = data.test, ncomp= n)
      pcr.errors[j,n] <- mean((y.pred-data.test$cnt)^2)
    }
  }
  # choose v for which mean is the lowest
  pcr.mean <- rep(0,11)
  for(n in 1:nb.variables.max){
    pcr.mean[n] <- mean(pcr.errors[,n])
  }
  pcr.nopt <- min(which(min(pcr.mean) == pcr.mean)) # si on choisit la plus petite moyenne
  err.pcr.nopt[i] <- pcr.nopt
  
  # validation our model with best model 
  data.train <- data.inner
  data.test <- data.validation
  # modeling with regsubset
  best.model <- pcr(cnt~., data=data.train, scale=TRUE, validation="CV")
  # prediction
  y.pred<- predict(best.model, data.test, ncomp = pcr.nopt)
  err.pcr.mse[i] <- mean((y.pred-data.test$cnt)^2)
}
mean(err.pcr.mse)

######################
#Selection ascendante et descendate
######################
predict.regsubsets =function (object ,newdata ,id ,...){
  form <- as.formula(object$call[[2]])
  mat <- model.matrix(form,newdata)
  coefi <- coef(object ,id=id)
  xvars <- names(coefi)
  mat[,xvars]%*%coefi
}

library(leaps)

nb.variables.max <- ncol(data)-1
err.sbf.mse<- rep(0, 10)
err.sbf.vopt <- rep(0,10)

err.sbb.mse<- rep(0, 10)
err.sbb.vopt <- rep(0,10)

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,] 
  data.validation <- data[index.outer.cv,] 
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  subset.backward.errors <- matrix(NA, 10,  nb.variables.max)
  subset.forward.errors <- matrix(NA, 10,  nb.variables.max)
  subset.exhaustive.errors <- matrix(NA, 10,  nb.variables.max)
  for(j in 1:10){ 
    index.inner.cv <- idx.test.inner[[i]][[j]] 
    data.train <- data[-index.inner.cv,]
    data.test <- data[index.inner.cv, ]
    # train regsubset model for backward and forward method
    model.backward <- regsubsets(cnt~., data=data.train, nvmax=nb.variables.max, 
                                 method="backward")
    model.forward <- regsubsets(cnt~., data=data.train, nvmax=nb.variables.max, 
                                method="forward")

    # predict model for both method and for different number of the best variables taken in considerations
    for(v in 1:nb.variables.max){
      y.pred.backward <- predict(model.backward,data.test, id=v)
      subset.backward.errors[j,v] <- mean((y.pred.backward-data.test$cnt)^2)
      
      y.pred.forward <- predict(model.forward, data.test, id=v)
      subset.forward.errors[j,v] <- mean((y.pred.forward-data.test$cnt)^2)
          }
  }
  # choose v for which standard deviation is the lowest
  backward.mean <- rep(0,nb.variables.max)
  backward.sd <- rep(0,nb.variables.max)
  for(v in 1:nb.variables.max){
    backward.mean[v] <- mean(subset.backward.errors[,v])
    backward.sd[v] <- sd(subset.backward.errors[,v])/sqrt(length(subset.backward.errors[,v]))
  }
  backward.vopt <- min(which(min(backward.mean) == backward.mean)) # si on choisit la plus petite moyenne
  #backward.vopt <- which(min(backward.sd) == backward.sd) # si on choisit le plus petit écart type
  err.sbb.vopt[i] <- backward.vopt
  
  forward.mean <- rep(0,nb.variables.max)
  forward.sd <- rep(0,nb.variables.max)
  for(v in 1:nb.variables.max){
    forward.mean[v] <- mean(subset.forward.errors[,v])
    forward.sd[v] <- sd(subset.forward.errors[,v])/sqrt(length(subset.forward.errors[,v]))
  }
  forward.vopt <- min(which(min(forward.mean) == forward.mean)) # si on choisit la plus petite moyenne
  #forward.vopt <- which(min(forward.sd) == forward.sd) # si on choisit le plus petit écart type
  err.sbf.vopt[i] <- forward.vopt
  
  # validation our model with best model 
  data.train <- data.inner
  data.test <- data.validation
  # modeling with regsubset
  best.model.backward <- regsubsets(cnt~., data=data.train, nvmax=nb.variables.max, method="backward")
  # prediction
  y.pred.backward <- predict(best.model.backward, data.test, id=backward.vopt)
  err.sbb.mse[i] <- mean((y.pred.backward-data.test$cnt)^2)
  
  # modeling with regsubset
  best.model.forward <- regsubsets(cnt~., data=data.train, nvmax=nb.variables.max, method="forward")
  # prediction
  y.pred.forward <- predict(best.model.forward, data.test, id=forward.vopt)
  err.sbf.mse[i] <- mean((y.pred.forward-data.test$cnt)^2)
  
}
mean(err.sbb.mse)
mean(err.sbf.mse)

# # step-final, training best model 
# best.model <- regsubsets(cnt~., data=data, nvmax = 11, method = "backward")
# pred <- predict(best.models, data.test, id=v)
# # subset_id = 56 
# 
# jpeg("subset_backward.jpg", width=2000, height=800, res=60)
# reg.fit<-regsubsets(cnt~., data=data,method='backward',nvmax=nv_var_opt)
# plot(reg.fit,scale="r2")
# dev.off()
# sum.ret.full<-summary(reg.fit)
# 
# jpeg("r2_nbvariables.jpg", width=1000, height=400, res=70)
# plot(sum.ret.full$rsq,xlab='Nb. de variables',ylab='R^2',type='l')
# dev.off()


#####################
#Splines
#####################
#Splines naturel
err.splinesnaturel.mse <- rep(0,10)
p.sequences <- seq(1,10, by=1)


for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  #Search best p for Random Forest
  sp.pmin <- rep(0, length(p.sequences))
  for(p in 1:length(p.sequences)){
    for(j in 1:10){
      index.inner.cv <- idx.test.inner[[i]][[j]] 
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      model.spline <- lm(cnt ~.+ns(atemp, p.sequences[p])+ns(windspeed,p.sequences[p])+ns(hum, p.sequences[j]), data=data.train) 
      model.pred <- predict(model.spline,newdata=data.test)
      sp.pmin[p] <-  sp.pmin[p] + mean((data.test$cnt -model.pred)^2)
    }
  }
  idx.pmin <- which(min(sp.pmin) == sp.pmin)
  best.pmin <- p.sequences[idx.pmin]
  
  data.train <- data.inner
  data.test <- data.validation
  
  naturalspline <- lm(cnt~.+ ns(atemp, best.pmin )+ ns(windspeed, best.pmin)+ns(hum,best.pmin ), data=data.train)
  pred <- predict(naturalspline, newdata=data.test)
  err.splinesnaturel.mse[i] = mean((data.test$cnt -pred)^2)
  
}
mean(err.splinesnaturel.mse)

#Smoothing splines

err.splinessmooth.mse <- rep(0,10)
p.sequences <- seq(1,10, by=1)


for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  #Search best p for Random Forest
  sp.pmin <- rep(0, length(p.sequences))
  for(p in 1:length(p.sequences)){
    for(j in 1:10){
      index.inner.cv <- idx.test.inner[[i]][[j]] 
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      model.spline <- lm(cnt ~.+s(atemp, p.sequences[p])+s(windspeed,p.sequences[p])+s(hum, p.sequences[j]), data=data.train) 
      model.pred <- predict(model.spline,newdata=data.test)
      sp.pmin[p] <-  sp.pmin[p] + mean((data.test$cnt -model.pred)^2)
    }
  }
  idx.pmin <- which(min(sp.pmin) == sp.pmin)
  best.pmin <- p.sequences[idx.pmin]
  
  data.train <- data.inner
  data.test <- data.validation
  
  naturalspline <- lm(cnt~.+ s(atemp, best.pmin )+ s(windspeed, best.pmin)+s(hum, best.pmin), data=data.train)
  pred <- predict(naturalspline, newdata=data.test)
  err.splinessmooth.mse[i] = mean((data.test$cnt -pred)^2)
  
}
mean(err.splinessmooth.mse)


#####################
#GAM
#####################


err.gam.mse <- rep(0,10)
p.sequences <- seq(1,10, by=1)


for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  #Search best p for Random Forest
  sp.pmin <- rep(0, length(p.sequences))
  for(p in 1:length(p.sequences)){
    for(j in 1:10){
      index.inner.cv <- idx.test.inner[[i]][[j]] 
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      model.gam <- gam(cnt ~.+s(atemp, p.sequences[p])+s(windspeed,p.sequences[p])+s(hum, p.sequences[j]), data=data.train) 
      model.pred <- predict(model.gam,newdata=data.test)
      sp.pmin[p] <-  sp.pmin[p] + mean((data.test$cnt -model.pred)^2)
    }
  }
  idx.pmin <- which(min(sp.pmin) == sp.pmin)
  best.pmin <- p.sequences[idx.pmin]
  
  data.train <- data.inner
  data.test <- data.validation
  
  model.gam <- gam(cnt~.+ s(atemp, best.pmin )+ s(windspeed, best.pmin)+s(hum, best.pmin), data=data.train)
  pred <- predict(model.gam, newdata=data.test)
  err.gam.mse[i] = mean((data.test$cnt -pred)^2)
  
}
mean(err.gam.mse)

#####################
##Decision tree
#####################

library(rpart.plot)
err.tree.mse <- rep(0,10)

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]

  
  data.train <- data.inner
  data.test <- data.validation
  
  model.tree <- rpart(cnt~., data=data.train, method="anova")
  pred <- predict(model.tree, newdata=data.test)
  err.tree.mse[i] = mean((data.test$cnt -pred)^2)
  
}
mean(err.tree.mse)
# idx.train <- sample(nrow(data)) 
# tree <- rpart(cnt~., data=data,subset = idx.train, method="anova")
rpart.plot(model.tree, box.palette="RdBu", shadow.col="gray", fallen.leaves=FALSE)

#####################
# Bagging
#####################
#arbre
library(randomForest)

err.bag.mse<- rep(0, 10)

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,] 
  data.validation <- data[index.outer.cv,] 
  # validation our model with best model 
  data.train<- data.inner
  data.test <- data.validation
  
  # modeling with randomForest
  model.fit <- randomForest(cnt~., data=data.train, mtry = ncol(data)-1)
  summary(model.fit)
  
  # prediction
  ypredi <- predict(model.fit, newdata=data.test)
  err.bag.mse[i] = mean((data.test$cnt -ypredi)^2)
}
boxplot(err.bag.mse)
mean(err.bag.mse) # 3551.154


##################
# Random Forest
##################

# nested CV - Random forest 
library(randomForest)

err.nrf.mse<- rep(0, 10)
err.nrf.pmin <- rep(0,10)
p.sequences <- c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
#Verifier à quoi sert le p
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,] 
  data.validation <- data[index.outer.cv,] 
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  rdf.mse.pmin <- rep(0,length(p.sequences))
  for(p in 1:length(p.sequences)){ 
    for(j in 1:10){ 
      index.inner.cv <- idx.test.inner[[i]][[j]]
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      
      model.rdf <- randomForest(cnt~., data=data.train, ntree = 100, mtry=p.sequences[p])
      print(p.sequences[p])
      y.pred <- predict(model.rdf, newdata=data.test)
      
      rdf.mse.pmin[p] <- rdf.mse.pmin[p] + mean((data.test$cnt -y.pred)^2)  # on compare les sommes ou les additions, c'est pareil car 10 exemplaires
    }
  } 
  idx.pmin <- which(min(rdf.mse.pmin) == rdf.mse.pmin)  # on récupère l'index qui correspond à la plus petite MSE en moyenne
  best.pmin <- p.sequences[idx.pmin]  # on récupère le K qui correspond à cet index
  err.nrf.pmin[i] <- best.pmin
  
  # validation our model with best model 
  data.train.x <- data.inner[, -ncol(data)]
  data.train.y <- data.inner[, ncol(data)]
  data.test.x <- data.validation[, -ncol(data)]
  data.test.y <- data.validation[, ncol(data)]
  
  # modeling with randomForest
  model.fit <- randomForest(data.train.y~., data=data.train.x, mtry = best.pmin)
  summary(model.fit)
  
  # prediction
  ypredi <- predict(model.fit, newdata=data.test.x)
  mean((data.test.y -ypredi)^2)
  err.nrf.mse[i] = mean((data.test.y -ypredi)^2)
}
boxplot(err.nrf.mse)
mean(err.nrf.mse) # 4670


######################
#SVM
#####################
library(kernlab)
library(MASS)
#Gaussian kernel
err.svmgauss.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]

  
  y.test <- data.validation[, ncol(data)]
  svmfit<-ksvm(cnt~.,data=data.inner,scaled=TRUE,type="eps-svr", kernel="rbfdot",C=1)
  yhat<-predict(svmfit,newdata=data.validation) 
  err.svmgauss.mse[i] <- mean((y.test - yhat)^2)
}
boxplot(err.svmgauss.mse)
mean(err.svmgauss.mse)

#polynom kernel
err.svmpolyn.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  
  y.test <- data.validation[, ncol(data)]
  svmfit<-ksvm(cnt~.,data=data.inner,scaled=TRUE,type="eps-svr", kernel="polydot",C=10)
  yhat<-predict(svmfit,newdata=data.validation) 
  err.svmpolyn.mse[i] <- mean((y.test - yhat)^2)
}
boxplot(err.svmpolyn.mse)
mean(err.svmpolyn.mse)

#linear kernel
err.svmlin.mse <-  rep(0, 10)
for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  
  y.test <- data.validation[, ncol(data)]
  svmfit<-ksvm(cnt~.,data=data.inner,scaled=TRUE,type="eps-svr", kernel="vanilladot",C=10,epsilon=0.1)
  yhat<-predict(svmfit,newdata=data.validation) 
  err.svmlin.mse[i] <- mean((y.test - yhat)^2)
}
boxplot(err.svmlin.mse)
mean(err.svmlin.mse)

#laplacien kernel
err.svmlaplace.mse <- rep(0,10)
c.sequences <- c(0.001, 0.01, 0.1, 1, 10, 100, 1000, 10e4)

for(i in 1:10){
  index.outer.cv <- idx.test.outer[[i]]
  data.inner <- data[-index.outer.cv,]
  data.validation <- data[index.outer.cv,]
  
  # re-sampling (inner cross validation)
  data.inner <- data.inner[rs.data.inner[[i]], ]
  #Search best p for Random Forest
  cmin <- rep(0, length(c.sequences))
  for(c in 1:length(c.sequences)){
    for(j in 1:10){
      index.inner.cv <- idx.test.inner[[i]][[j]]
      data.train <- data.inner[-index.inner.cv, ]
      data.test <- data.inner[index.inner.cv, ]
      model.svm <- ksvm(cnt~.,data=data.inner,scaled=TRUE,type="eps-svr", kernel="laplacedot",C=c.sequences[c])
      model.pred <- predict(model.svm,newdata=data.test)
      cmin[c] <-  cmin[c] + mean((data.test$cnt - model.pred)^2)
    }
  }
  idx.cmin <- which(min(cmin) == cmin)
  best.cmin <- c.sequences[idx.cmin]
  
  data.train <- data.inner
  data.test <- data.validation
  
  model.svm <- ksvm(cnt~.,data=data.inner,scaled=TRUE,type="eps-svr", kernel="laplacedot",C=c.sequences[best.cmin])
  model.pred <- predict(model.svm,newdata=data.test)
  

  err.svmlaplace.mse[i] = mean((data.test$cnt -model.pred)^2)
  
}
mean(err.svmlaplace.mse)


#####################
#Réseau de neurone
#####################
p <- ncol(data) - 1
train <- data[,-(p+1)]
test <-data[,p+1]

# loading package
library(keras)
library(tensorflow)
library(magrittr)
model <- keras::keras_model_sequential() %>%
  keras::layer_dense(units = 170, activation = "relu", input_shape = 256) %>%
  keras::layer_dense(units = 170, activation = "relu") %>%
  keras::layer_dense(units = 5, activation = "softmax") %>%
  keras::compile(loss = "categorical_crossentropy", optimizer = "adam", metrics = "accuracy")

model %>% fit(as.matrix(train), keras::to_categorical(as.numeric(train$cnt))[,-1], epochs = 50, batch_size = 32)
model %>% evaluate(as.matrix(test), keras::to_categorical(as.numeric(test$cnt))[,-1])


##################
# SUBSET SELECTION
##################

# # errors cross-validation 
# subset.errors <- matrix(NA, 100, 10)
# library(leaps) # library to selection of variables 
# for(i in 1:10){
#   index.outer.cv <- idx.test.outer[[i]]
#   data.train <- data[-index.outer.cv,]
#   data.test <- data[index.outer.cv, ]
#   best.models <- regsubsets(cnt~., data=data.train, nvmax=nb.variables.max, 
#                             method="backward")
#   for(v in 1:nb.variables.max){
#     pred <- predict(best.models, data.test, id=v)
#     subset.errors[v,i] <- mean((pred-data.test$y)^2)
#   }
#   
# }
# 
# forward.mean = rep(0,100)
# for(v in 1:nb.variables.max){
#   forward.mean[v] <- mean(subset.errors[v,])
# }
# boxplot(subset.errors)
# boxplot(subset.errors, ylim=c(0, 900))
# plot.cv.error(as.matrix(subset.errors))
# nb_var_opt = 56
# 
# err.sub.mse = subset.errors[,nb_var_opt]
# best.models <- regsubsets(y~., data=data.train, nvmax=nb.variables.max, 
#                           method="forward")
# 
# nb.variables.max <- 100
# # function prediction taken of
# # "An introduction to statistical learning"
# predict.regsubsets =function (object ,newdata ,id ,...){
#   form <- as.formula(object$call[[2]])
#   mat <- model.matrix(form,newdata)
#   coefi <- coef(object ,id=id)
#   xvars <- names(coefi)
#   mat[,xvars]%*%coefi 
# }

# nested CV - regsubset 




####################
# FEATURE EXTRACTION
####################
# X = data[,-12]
# X<-scale(X)
# pca<-princomp(X)
# Z<-pca$scores
# lambda<-pca$sdev^2
# pairs(Z[,1:3],col=data[,12],pch=data[,12])
# 
# jpeg("pca.jpg", width=1000, height=600, res=80)
# plot(cumsum(lambda)/sum(lambda),type="l",xlab="nombre de composantes",ylab="proportion de la variance expliquée")
# dev.off()
# 
# 
# library(pls)
# pcr.fit<-pcr(cnt~.,data=data,scale=TRUE,validation="CV")
# summary(pcr.fit)
# validationplot(pcr.fit,val.type = "MSEP",legendpos = "topright",xlab="nombre de composantes", title="MSE en fonction du nombre de composante")
# 
# y.pred <- predict(pcr.fit, newdata = data.test, ncomp= 100)
# mean((y.pred-data.test$y)^2)



# errors 
errors <- matrix(0, 10, 17)
errors[,1] <- err.lin.mse
errors[,2] <- err.ela.mse
errors[,3] <- err.las.mse
errors[,4] <- err.rid.mse
errors[,5] <- err.pcr.mse
errors[,6] <- err.sbf.mse
errors[,7] <- err.sbb.mse
errors[,8] <- err.splinesnaturel.mse
errors[,9] <- err.splinessmooth.mse
errors[,10] <- err.gam.mse
errors[,11] <- err.tree.mse
errors[,12] <- err.bag.mse
errors[,13] <- err.nrf.mse
errors[,14] <- err.svmgauss.mse
errors[,15] <- err.svmpolyn.mse
errors[,16] <- err.svmlin.mse
errors[,17] <- err.svmlaplace.mse
  

plot.cv.error(errors[], x.title = "Modeles", ylim = c(200000, 690000))
text(x=1, y=mean(err.lin.mse)+60000, labels="LR")
text(x=2, y=mean(err.ela.mse)+64000, labels="Elnet")
text(x=3, y=mean(err.las.mse)+64000, labels="LAS")
text(x=4, y=mean(err.rid.mse)+64000, labels="RID")
text(x=5, y=mean(err.pcr.mse)+63000, labels="PCR")
text(x=6, y=mean(err.sbf.mse)+92000, labels="SBF")
text(x=7, y=mean(err.sbb.mse)+75000, labels="SBB")
text(x=8, y=mean(err.splinesnaturel.mse)+40000, labels="NatSp")
text(x=9, y=mean(err.splinessmooth.mse)+58000, labels="SmoSp")
text(x=10, y=mean(err.gam.mse)+45000, labels="GAM")
text(x=11, y=mean(err.tree.mse)+95000, labels="TREE")
text(x=12, y=mean(err.bag.mse)+55000, labels="BAG_TREE")
text(x=13, y=mean(err.nrf.mse)+55000, labels="RF")
text(x=14, y=mean(err.svmgauss.mse)+50000, labels="SVM_Gauss")
text(x=15, y=mean(err.svmpolyn.mse)+60000, labels="SVM_poly")
text(x=16, y=mean(err.svmlin.mse)+60000, labels="SVM_lin")
text(x=17, y=mean(err.svmlaplace.mse)+50000, labels="SVM_lapl")

tableau_final <- data.frame(cnt=c(mean(err.lin.mse),
                                  mean(err.ela.mse),
                                  mean(err.las.mse),
                                  mean(err.rid.mse),
                                  mean(err.pcr.mse),
                                  mean(err.sbf.mse),
                                  mean(err.sbb.mse),
                                  mean(err.splinesnaturel.mse),
                                  mean(err.splinessmooth.mse),
                                  mean(err.gam.mse),
                                  mean(err.tree.mse),
                                  mean(err.bag.mse),
                                  mean(err.nrf.mse),
                                  mean(err.svmgauss.mse),
                                  mean(err.svmpolyn.mse),
                                  mean(err.svmlin.mse),
                                  mean(err.svmlaplace.mse)))

rownames(tableau_final)<- c("LR","Elnet","LAS","RID","PCR", "SBF","SBB","NatSp","SmoSp","GAM","TREE","BAG_TREE",
                      "RF","SVM_Gauss","SVM_poly","SVM_lin","SVM_lapl")

library(gridExtra)
library(grid)

grid.table(tableau_final)


####################
#Réseau de neurone
####################
# library(nnet)
# idx.train <- sample(nrow(data), floor(2/3*nrow(data)), replace=FALSE)
# data.train <- data[idx.train,]
# data.test <- data[-idx.train,]
# 
# nn3<- nnet(cnt~., data=data.train, size=45, linout = TRUE, decay=0)
# pred3<- predict(nn3,newdata=data.test)
# mean((pred3-data.test$cnt)^2)


#lm(y~X1*X2, data=data)

#PCA
# 
# data <- read.table("data/letters_train.txt")
# data.train <- data[1:8000, ]
# data.test <- data[8001:10000, ]
# ncomposantes <- 5
# # training the model
# X <- scale(as.matrix(data.train[, -1]))
# pca <- princomp(X, scores=TRUE)
# Z <- pca$scores[,1:ncomposantes]
# # pca$loadings --> env.RDA
# # testing data
# X <- scale(as.matrix(data.test[, -1]))
# Z <- X %*% pca$loadings[, 1:ncomposantes]
